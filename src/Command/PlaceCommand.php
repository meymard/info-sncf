<?php

namespace Meymard\InfoSncf\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

#[AsCommand(name: 'place', description: 'Place command')]
class PlaceCommand extends Command implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $navitia = $this->container->get('navitia_component');
        $navitia->setConfiguration([
            'url' => 'api.navitia.io',
            'token' => 'e28dc936-3384-43ff-977f-ee28c83ef4b5',
        ]);
        $output->writeln('This is a test');

        $query = [
            'api' => 'journeys',
            'parameters' => [
                'from' => '2.3749036;48.8467927',
                'to' => '2.2922926;48.8583736',
            ],
        ];

        $result = $navitia->call($query);
        var_dump($result);

        return 10;
    }
}
